import React from 'react'



export default function Item_Shoe({ shoe, handleAddCard }) {
    return (
        <div className="col-4">
            <div className="card h-100">
                <img src={shoe.image} className="card-img-top" alt="..." />
                <div className="card-body">
                    <h5 className="card-title">{shoe.name}</h5>
                    <p className="card-text">{shoe.price}</p>
                    <a
                        onClick={() => {
                            handleAddCard(shoe);
                        }}
                        href="#"
                        className="btn btn-primary"
                    >
                        Add
                    </a>
                </div>
            </div>
        </div>

    )
}
