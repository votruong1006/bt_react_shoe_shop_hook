import React, { useState } from "react";
import Cart_Shoe from "./Cart_Shoe";
import { data_shoe } from "./Data_Shoe";
import List_shoe from "./List_shoe";

export default function Shoe_Shop_Hook() {
    const [shoe, setShoe] = useState({ listShoe: data_shoe });
    const [cart, setCart] = useState([]);
    let handleAddCard = (shoe) => {
        let cloneCard = [...cart];
        let index = cloneCard.findIndex((item) => {
            return item.id == shoe.id;
        });
        if (index == -1) {
            let newShoe = { ...shoe, soLuong: 1 };
            cloneCard.push(newShoe);
        } else {
            cloneCard[index].soLuong++;
        }
        setCart(cloneCard);
    };
    let handleDelete = (idshoe) => {
        let newCard = cart.filter((item) => {
            return item.id != idshoe;
        });
        setCart(newCard);
    };
    let handleChangeQuatity = (idShoe, soLuongg) => {
        let cloneCard = [...cart];
        let index = cloneCard.findIndex((item) => {
            return item.id == idShoe;
        });
        cloneCard[index].soLuong = cloneCard[index].soLuong + soLuongg;
        setCart(cloneCard);
    };

    return (
        <div className="container">
            <h2 className="text-primary">Shoe_Shop_Hook</h2>
            <div className="row">
                {cart.length > 0 && <div className="col-8">
                    <Cart_Shoe
                        handleChangeQuatity={handleChangeQuatity}
                        handleDelete={handleDelete}
                        cart={cart}
                    />
                </div>}
                <div className="col-4">
                    <List_shoe handleAddCard={handleAddCard} listShoe={shoe.listShoe} />
                </div>

            </div>
        </div>
    );
}
