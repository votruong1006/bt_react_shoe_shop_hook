import React from 'react'
import Item_Shoe from './Item_Shoe'

export default function List_shoe({ listShoe, handleAddCard }) {
    return (
        <div>
            <h2 className='text-danger'>List_Shoe</h2>
            <div className='row'>{listShoe.map((item) => { return <Item_Shoe handleAddCard={handleAddCard} shoe={item} /> })}</div>
        </div>
    )
}
